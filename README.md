# A "Good Guys" Journal List
My non-comprehensive collection of "good guys" journals within my field of research (bioinformatics, computational biology, immunoinformatics, molecular biology, and related).
Publication fees were not evaluated, and thus some journals on this list charge exorbitant fees.
Where relevant, more information is provided, e.g. on open access, scope, criteria, impact.

Journal metrics have been extracted from [SCImago](http://www.scimagojr.com/journalrank.php?order=sjr&ord=desc), where available (SCImago is based on on Scopus data [owned by Elsevier]. If anyone can point me to something similar but more open I would happily include/replace it).

*This list is non-comprehensive, work in progress, and I do not endorse per se any of the journals mentioned explicitly.*
*You should investigate on your own if you agree with the editorial policies of individual journals.*
*I am also not implying that you should only publish in the journals on this list (neither am I).* 
*I also do not condemn any journal not mentioned, neither do I imply that they are not "good guys".*
*Make up your own mind what is important to you.*

## Contents
- By Publisher/Society
	- [American Association for the Advancement of Science](#american-association-for-the-advancement-of-science)
	- [American Society for Microbiology](#american-society-for-microbiology)
	- [Cambridge University Press](#cambridge-university-press)
	- [Cold Spring Harbor Laboratory Press](#cold-spring-harbor-laboratory-press)
	- [eLife Sciences Publications](#elife-sciences-publications)
	- [EMBO press](#embo-press)
	- [FEMS](#fems)
    - [Genetics Society of America](#genetics-society-of-america)
	- [National Academy of Sciences](#national-academy-of-sciences)
	- [Oxford University Press](#oxford-university-press)
	- [Public Library of Science](#public-library-of-science)
	- [Royal Society](#royal-society)
	- [The American Association of Immunologists](#the-american-association-of-immunologists)
	- [The Company of Biologists](#the-company-of-biologists)
- By Journal
	- [Bioinformatics](#bioinformatics)
	- [Biology Letters](#biology-letters)
	- [Biology Open](#biology-open)
	- [Briefings in Bioinformatics](#briefings-in-bioinformatics)
	- [Briefings in Functional Genomics](#briefings-in-functional-genomics)
	- [Database](#database)
	- [DNA Research](#dna-research)
	- [eLIFE](#elife)
	- [EMBO Journal](#the-embo-journal)
	- [EMBO Molecular Medicine](#embo-molecular-medicine)
	- [EMBO Reports](#embo-reports)
    - [G3 Genes Genomes Genetics](#g3-genes-genomes-genetics)
    - [GENETICS](#genetics)
	- [Genetics Research](#genetics-research)
	- [Genome](#genome)
	- [Genome Biology and Evolution](#genome-biology-and-evolution)
	- [Genome Research](#genome-research)
	- [GigaScience](#gigascience)
	- [ImmunoHorizons](#immunohorizons)
	- [Journal of Cell Science](#journal-of-cell-science)
	- [Journal of Immunology](#journal-of-immunology)
	- [Journal of the Royal Society Interface](#journal-of-the-royal-society-interface)
	- [Life Science Alliance](#life-science-alliance)
	- [Molecular Biology and Evolution](#molecular-biology-and-evolution)
	- [Molecular Systems Biology](#molecular-systems-biology)
	- [mBio](#mbio)
	- [mSphere](#msphere)
	- [mSystems](#msystems)
	- [Nucleic Acids Research](#nucleic-acids-research)
	- [Open Biology](#open-biology)
	- [PLoS Biology](#plos-biology)
	- [PLoS Computational Biology](#plos-computational-biology)
	- [PLoS Genetics](#plos-genetics)
	- [PLoS Medicine](#plos-medicine)
	- [PLoS ONE](#plos-one)
	- [PLoS Pathogens](#plos-pathogens)
	- [PNAS](#pnas)
	- [RNA](#rna)
	- [Royal Society Open Science](#royal-society-open-science)
	- [Science](#science)
	- [Science Advances](#science-advances)
	- [Science Immunology](#science-immunology)

## [American Association for the Advancement of Science](https://en.m.wikipedia.org/wiki/American_Association_for_the_Advancement_of_Science)


### [Science](http://science.sciencemag.org/)
- OA: OA after 12 months; open access to research funded by the Gates Foundation
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=23571&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=23571" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Science Immunology](http://immunology.sciencemag.org/)
- OA: OA after 12 months; open access to research funded by the Gates Foundation

### [Science Advances](http://advances.sciencemag.org/)
- OA: fully OA
- Scope:
- Impact:

## [American Society for Microbiology](https://www.asm.org/)

### [mBio](http://mbio.asm.org/)
- OA: Fully OA. APC $2300 for Research Articles and $1200 for the shorter Observation and Opinions & Hypotheses formats
- Scope: The scope of mBio reflects the enormity of the microbial world, a highly interconnected biosphere where microbes interact with living and nonliving matter to produce outcomes that range from symbiosis to pathogenesis, energy acquisition and conversion, climate change, geologic change, food and drug production, and even animal behavioral change.
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19700188403&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19700188403" alt="SCImago Journal &amp; Country Rank"  /></a>

### [mSphere](http://msphere.asm.org)
- OA: Fully OA. Fees potentially waived. mSphere non-member APCs are $3150 for Research Articles and Resource Reports and $1800 for the shorter Observation and Opinion/Hypothesis formats
- Scope: mSphere is a multidisciplinary open-access journal that publishes high-quality work that makes fundamental contributions to any of the immense range of fields within the microbial sciences.
- Impact:
<br /><a href="https://www.scimagojr.com/journalsearch.php?q=21100850718&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="https://www.scimagojr.com/journal_img.php?id=21100850718" alt="SCImago Journal &amp; Country Rank"  /></a>

### [mSystems](http://msystems.asm.org/)
- OA: Fully OA. Non-member APCs are $3150 for Research Articles, Resource Reports, and Methods and Protocols and $1800 for the shorter Observation and Opinion/Hypothesis formats.
- Scope: Publishes preeminent work that stems from applying technologies for high-throughput analyses to achieve insights into the metabolic and regulatory systems at the scale of both the single cell and microbial communities. The scope of mSystems encompasses all important biological and biochemical findings drawn from analyses of large data sets, as well as new computational approaches for deriving these insights. mSystems welcomes submissions from researchers who focus on the microbiome, genomics, metagenomics, transcriptomics, metabolomics, proteomics, glycomics, bioinformatics, and computational microbiology. 
- Impact:
<br /><a href="https://www.scimagojr.com/journalsearch.php?q=21100850723&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="https://www.scimagojr.com/journal_img.php?id=21100850723" alt="SCImago Journal &amp; Country Rank"  /></a>

## [Cambridge University Press](https://www.cambridge.org/core/browse-subjects/life-sciences)

### [Genetics Research](https://www.cambridge.org/core/journals/genetics-research)
- OA: Open access option available
- Scope: The journal focuses on the use of new technologies, such as massive parallel sequencing together with bioinformatics analysis, to produce increasingly detailed views of how genes function in tissues and how these genes perform, individually or collectively, in disease aetiology
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=22172&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=22172" alt="SCImago Journal &amp; Country Rank"  /></a>


## [Cold Spring Harbor Laboratory Press](https://en.m.wikipedia.org/wiki/Cold_Spring_Harbor_Laboratory_Press)

### [Genome Research](http://genome.cshlp.org/site/misc/about.xhtml)
- OA: OA after 6 month. OA option available (+$2500)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=22214&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=22214" alt="SCImago Journal &amp; Country Rank"  /></a>

### [RNA](http://rnajournal.cshlp.org/site/misc/about.xhtml)
- OA: OA after 12 month. OA option available (+$2000)
- Scope: "Bioinformatics" describe computer-based analyses of sequence data or new informatic tools of interest to RNA scientists.
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19017&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19017" alt="SCImago Journal &amp; Country Rank"  /></a>

## eLife Sciences Publications
### [eLIFE](https://elifesciences.org/about)
- OA: Author-pays OA ($2500)
- Scope: eLife publishes important research in all areas of the life and biomedical sciences
- Impact:
<br/><a href="http://www.scimagojr.com/journalsearch.php?q=21100242814&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100242814" alt="SCImago Journal &amp; Country Rank"  /></a>


## [EMBO press](http://www.embo.org/embo-press)

### [Life Science Alliance](http://www.life-science-alliance.org)
- OA: Author-pays OA ($2400)

### [Molecular Systems Biology](http://msb.embopress.org/about)
- OA: Author-pays OA ($4200)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=4700152228&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=4700152228" alt="SCImago Journal &amp; Country Rank"  /></a>

### [The EMBO Journal](http://emboj.embopress.org/about)
- OA: Author-pays OA ($5200)
- Impact:
<br/><a href="http://www.scimagojr.com/journalsearch.php?q=17435&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=17435" alt="SCImago Journal &amp; Country Rank"  /></a>

### [EMBO Reports](http://embor.embopress.org/about)
- OA: Author-pays OA ($5200)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=22088&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=22088" alt="SCImago Journal &amp; Country Rank"  /></a>

### [EMBO Molecular Medicine](http://embomolmed.embopress.org/about)
- OA: Author-pays OA ($4200)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19600166310&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19600166310" alt="SCImago Journal &amp; Country Rank"  /></a>

## [FEMS](https://fems-microbiology.org/about_fems/network-and-activities/journals/)

### [FEMS Microbiology Ecology](https://academic.oup.com/femsec)

### [FEMS Microbiology Letters](https://academic.oup.com/femsle)

### [FEMS Microbiology Reviews](https://academic.oup.com/femsre)

### [Pathogens and Disease](https://academic.oup.com/femspd)


## [Genetics Society of America](http://genetics-gsa.org/)

### [GENETICS](http://www.genetics.org/)
- OA: Author's choice = Members of the Genetics Society of America can choose the open access option for an additional charge of $1500, or if a non-member for $2000 + $100 per page.
- Scope: GENETICS has published high-quality, original research presenting novel findings bearing on genetics and genomics. The journal publishes empirical studies of organisms ranging from microbes to humans, as well as theoretical work.
- Impact:
<a href="http://www.scimagojr.com/journalsearch.php?q=22173&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=22173" alt="SCImago Journal &amp; Country Rank"  /></a>

### [G3 Genes Genomes Genetics](http://www.g3journal.org/)
- OA: Fully open access ($1815 - $2145)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21100232417&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100232417" alt="SCImago Journal &amp; Country Rank"  /></a>

## [National Academy of Sciences](https://en.wikipedia.org/wiki/National_Academy_of_Sciences)

### [PNAS](http://www.pnas.org/site/aboutpnas/index.xhtml)
- OA: OA after 6 month. Author-pays immediate open access (+$1450)
- Scope: multidisciplinary
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21121&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21121" alt="SCImago Journal &amp; Country Rank"  /></a>

## [NRC Research Press](http://www.nrcresearchpress.com/page/about)

### [Genome](http://www.nrcresearchpress.com/journal/gen)
- OA: Author-pays OA (+$3000)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=22211&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=22211" alt="SCImago Journal &amp; Country Rank"  /></a>


## [Oxford University Press](http://oxfordindex.oup.com/browse?pageSize=100&sort=titlesort&t1=SCI00960&type_0=journal)

### [Nucleic Acids Research](https://academic.oup.com/nar)
- OA: Author-pays OA ($2670)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=14204&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=14204" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Bioinformatics](https://academic.oup.com/bioinformatics)
- OA: Author-pays OA (+$3000)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=17945&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=17945" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Molecular Biology and Evolution](https://academic.oup.com/mbe)
- OA: Author-pays OA ($2500)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=12216&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=12216" alt="SCImago Journal &amp; Country Rank"  /></a>

### [DNA Research](https://academic.oup.com/dnaresearch)
- OA: Author-pays OA (+$769)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=17426&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=17426" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Briefings in Bioinformatics](https://academic.oup.com/bib)
- OA: Author-pays OA (+$3150)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=17956&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=17956" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Briefings in Functional Genomics](https://academic.oup.com/bfg)
- OA: Author-pays OA ($3150)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19700175823&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19700175823" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Genome Biology and Evolution](https://academic.oup.com/gbe)
- OA: Author-pays OA ($1800)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19700182013&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19700182013" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Database](https://academic.oup.com/database)
- OA: Author-pays OA (+$1600)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=19700188321&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=19700188321" alt="SCImago Journal &amp; Country Rank"  /></a>

### [GigaScience](https://academic.oup.com/gigascience)
- OA: Author-pays OA ()
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21100420802&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100420802" alt="SCImago Journal &amp; Country Rank"  /></a>

## [Public Library of Science](https://en.m.wikipedia.org/wiki/PLOS)

### [PLoS Computational Biology](http://journals.plos.org/ploscompbiol/s/journal-information)
- OA: Author-pays OA ($2250)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=4000151810&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=4000151810" alt="SCImago Journal &amp; Country Rank"  /></a>

### [PLoS Biology](http://journals.plos.org/plosbiology/s/journal-information)
- OA: Author-pays OA ($2900)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=12977&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=12977" alt="SCImago Journal &amp; Country Rank"  /></a>

### [PLoS Medicine](http://journals.plos.org/plosmedicine/s/journal-information)
- OA: Author-pays OA ($2900)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=144840&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=144840" alt="SCImago Journal &amp; Country Rank"  /></a>

### [PLoS Genetics](http://journals.plos.org/plosgenetics/)
- OA: Author-pays OA ($2250)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=4000151808&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=4000151808" alt="SCImago Journal &amp; Country Rank"  /></a>

### [PLoS Pathogens](http://journals.plos.org/plospathogens/s/journal-information)
- OA: Author-pays OA ($2250)
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=4000151809&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=4000151809" alt="SCImago Journal &amp; Country Rank"  /></a>

### [PLoS ONE](http://journals.plos.org/plosone/s/journal-information)
- OA: Author-pays OA ($1450)
- Scope: All fields of science
- Criteria: Perceived impact not a criteria
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=10600153309&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=10600153309" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Royal Society](https://en.m.wikipedia.org/wiki/Royal_Society)

### [Journal of the Royal Society Interface](http://rsif.royalsocietypublishing.org/about)
- OA: OA after 12 month. Open access options
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=4000151907&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=4000151907" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Open Biology](http://rsob.royalsocietypublishing.org/about)
- OA: fully open access
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21100206244&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100206244" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Biology Letters](http://rsbl.royalsocietypublishing.org/about)
- OA: open access options
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=145678&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=145678" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Royal Society Open Science](http://rsos.royalsocietypublishing.org/about)
- OA: fully open access, currently no fees for publishing
- Criteria: Perceived impact not a criteria
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21100446014&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100446014" alt="SCImago Journal &amp; Country Rank"  /></a>


## [The American Association of Immunologists](https://en.m.wikipedia.org/wiki/American_Association_of_Immunologists)

### [Journal of Immunology](http://www.jimmunol.org/info/journal)
- OA: After 12 month. Author-choice option +$3000 for immediate OA
- Scope: Many + "Systems Immunology"
- Criteria: Manuscripts submitted to the “Systems Immunology” section should center on analyses of novel large data sets, generated by the authors, which will serve as useful resources for future work in the field. The authors’ informatics analyses should allow them to draw concrete conclusions, supported by the data, about the biology of the system(s) under study. Alternatively, manuscripts may describe a novel method of data analysis, which could be applied to publicly available data sets. The latter type of manuscript must convincingly demonstrate the utility of the new analysis method to reveal novel biological insights about the system(s) under study. **However, it is not necessary for either type of manuscript to include definitive mechanistic analysis or experiments beyond those necessary for the acquisition of reproducible and meaningful data sets.**
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21275&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21275" alt="SCImago Journal &amp; Country Rank"  /></a>

### [ImmunoHorizons](http://www.immunohorizons.org)
- OA: Open access
- Scope: Adaptive Immunity, Clinical and Translational Immunology, Infectious Disease, Innate Immunity
- Criteria: **ImmunoHorizons will consider observations of interest even if complete mechanistic or functional characterization cannot yet be provided.** ImmunoHorizons will also consider for publication novel methods, assays or computational tools, as well as initial characterizations of novel reagents, including mouse strains, clones and antibodies, even if biological insights have not yet been fully realized. **Large descriptive data sets which are of interest to the field will also be considered.**


## [The Company of Biologists](https://en.m.wikipedia.org/wiki/The_Company_of_Biologists)

### [Journal of Cell Science](http://jcs.biologists.org/content/about)
- OA: Open access (varying options); publishing under the Green Open Access model is free (OA after 6 month).
- Scope: Full-range of cell biology
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=18556&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=18556" alt="SCImago Journal &amp; Country Rank"  /></a>

### [Biology Open](http://bio.biologists.org/content/about)
- OA: Full open access ($1495)
- Scope: Original research across all aspects of the biological sciences, including cell biology, developmental biology and experimental biology
- Criteria: Perceived impact not a criteria
- Impact:
<br /><a href="http://www.scimagojr.com/journalsearch.php?q=21100444312&amp;tip=sid&amp;exact=no" title="SCImago Journal &amp; Country Rank"><img border="0" src="http://www.scimagojr.com/journal_img.php?id=21100444312" alt="SCImago Journal &amp; Country Rank"  /></a>
